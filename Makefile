SRC  := src
TEST := test
TESTFILES := $(shell find test/ -name '*.test')

TARGET := marie-as

$(SRC)/%:
	$(MAKE) -C $(SRC) $*


$(TARGET): $(SRC)/$(TARGET)
	cp $< $@

.PHONY: test
test: $(TARGET) $(TESTFILES)

.PHONY: $(TESTFILES)
$(TESTFILES): $(TARGET)
	pipetest ./$< < $@

clean: $(SRC)/clean
	$(RM) $(TARGET)
