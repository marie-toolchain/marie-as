#pragma once
#include <argp.h>
#include <stdbool.h>

struct arguments {
	char *input_file;
	FILE *input_filep;
	bool debug;
  char *output_file;
};

struct argp argp;
struct arguments arguments;
