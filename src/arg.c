#include "arg.h"
#include <argp.h>
#include <stdbool.h>
#include <stdlib.h>


const char *argp_program_version =
  "PACKAGE VERSION";
const char *argp_program_bug_address =
  "<marco@sirabella.org>";

/* Program documentation. */
static char doc[] =
  "MARIE Assembler";

/* A description of the arguments we accept. */
static char args_doc[] = "[asmfile]";

/* The options we understand. */
static struct argp_option options[] = {
  {NULL, 'D', NULL,   0,  "produce assembler debugging messages" },
	{NULL, 'o', "FILE", 0,  "name the object-file output OBJFILE (default a.out)"},
  { 0 }
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;

	switch (key) {
		case ARGP_KEY_INIT: {
			/* defaults */
			arguments->debug = false;
			arguments->input_filep = stdin;
			arguments->output_file = "a.out";
			return 0;
		}
		case 'D':
			arguments->debug = true;
			return 0;
		case 'o':
			arguments->output_file = arg;
			return 0;

		case ARGP_KEY_ARG:
			if (state->arg_num >= 1)
				/* Too many arguments. */
				argp_usage (state);
			if (
			    arg == NULL ||
			    arg[0] == '\0' ||
			    (arg[0] == '-' && arg[1] == '\0')
			    ) {
				arguments->input_file = NULL;
				/* stick with the default filep */
			} else {
				arguments->input_file  =       arg;
				arguments->input_filep = fopen(arg, "r");
			}

			return 0;

		default:
			return ARGP_ERR_UNKNOWN;
	}
}

extern struct argp argp = { options, parse_opt, args_doc, doc };
extern struct arguments;

/*
 * to use:
 * argp_parse(&argp, argc, argv, 0, 0, &arguments);
 * from main
 */
