%{
 
#include <stdio.h>
#include "marie.tab.h"
%}

%option yylineno

%x oct
%x dec
%x hex

%%

"/".* { /* nada, comment */ }

(?i:"org")      { return K_ORG;      }

(?i:"jns")      { return K_JNS;      }
(?i:"load")     { return K_LOAD;     }
(?i:"store")    { return K_STORE;    }
(?i:"add")      { return K_ADD;      }
(?i:"subt")     { return K_SUBT;     }
(?i:"input")    { return K_INPUT;    }
(?i:"output")   { return K_OUTPUT;   }
(?i:"halt")     { return K_HALT;     }
(?i:"skipcond") { return K_SKIPCOND; }
(?i:"jump")     { return K_JUMP;     }
(?i:"clear")    { return K_CLEAR;    }
(?i:"addi")     { return K_ADDI;     }
(?i:"jumpi")    { return K_JUMPI;    }

(?i:"oct") { BEGIN(oct); return K_OCT; }
(?i:"dec") { BEGIN(dec); return K_DEC; }
(?i:"hex") { BEGIN(hex); return K_HEX; }

<oct>(?i:[0-7])+ {
	marielval.number = strtol(yytext, NULL, 8);
	BEGIN(INITIAL);
	return N_OCT;
};

<dec>-?(?i:[0-9])+ {
	marielval.number = strtol(yytext, NULL, 10);
	BEGIN(INITIAL);
	return N_DEC;
};

<hex>(?i:[0-9a-f])+ {
	marielval.number = strtol(yytext, NULL, 16);
	BEGIN(INITIAL);
	return N_HEX;
};

<*>[0-9](?i:[0-9a-f]*) { // has a leading number
	marielval.number = strtol(yytext, NULL, 16);
	return N_HEX;
};

(?i:[a-z_]+[a-z1-9_]*) {marielval.string = strdup(yytext); return TOKEN;}

<*>[[:blank:]] {}

<*>"\n" {return NEWLINE;}

. {return *yytext;}
