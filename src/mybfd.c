#include "mybfd.h"
#include <assert.h>
#include <bfd.h>
#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#ifdef NDEBUG
	#define DEBUG() printf("%s:%s:%i\n", __FILE__, __FUNCTION__, __LINE__)
#else
	#define DEBUG() {}
#endif
#define try(call) {if ((call) != TRUE) {printf("error on line %i: %s\n", __LINE__, bfd_errmsg(bfd_get_error())); abort();}}

#define add_section_flag(sec, FLAG) {flagword flags = bfd_get_section_flags (abfd, sec); flags |= (FLAG); try(bfd_set_section_flags (abfd, sec, flags));}

static bfd *abfd;


typedef struct { // to be used with realloc
	void *head;
	void *tail; // next writable pointer, should be incremented and checked against allocd_size when written
	void *allocd_tail; // final writable pointer, if going over this should realloc
} lengthed_array;

#define lengthed_array_init() {NULL, NULL, NULL}
#define lengthed_array_size(array) ((array).tail - (array).head)
#define lengthed_array_allocd_size(array) ((array).allocd_tail - (array).head)

#define typedef_ref(T, ref) (*(T **)&(ref))

static void resize_lengthed_array(lengthed_array *array, size_t size) {
	void *newhead= realloc(array->head, size);
	array->allocd_tail = newhead + (size);
	array->tail = lengthed_array_size(*array) + newhead;
	array->head = newhead;

	assert(array->tail <= array->allocd_tail);
	assert(array->head <= array->tail);
}

static void expand_lengthed_array(lengthed_array *array, size_t size) { // will not shrink
	if (size > lengthed_array_allocd_size(*array)) {
		// TODO: reallocs are expensive. do heuristics
		resize_lengthed_array(array, size);
	}
}

lengthed_array progdata = lengthed_array_init();
lengthed_array symtab = lengthed_array_init();
lengthed_array rela_text = lengthed_array_init();


asection *get_text_section() {
	static asection *ret = NULL; // just some simple memoization
	if (ret != NULL) {
		return ret;
	}
	asection *sec = bfd_make_section(abfd, ".text");
	ret = sec;
	return ret;
}



void append_symbol_to_symbtab(asymbol *symbol) {
	DEBUG();
	size_t size = sizeof symbol;

	expand_lengthed_array(&symtab, lengthed_array_size(symtab) + size);

	// this is done so that the increment (++) is done by the size of an asection // *
	// see: https://stackoverflow.com/a/38083528/6381767
	*typedef_ref(asymbol *, symtab.tail)++ = symbol; // wow
}

asymbol *lookup_symbol(const char *var) { // can't return double poiter, might change from under us
	for (asymbol **sym = symtab.head; sym < (asymbol **)symtab.tail; sym++) {
		if (strcmp((*sym)->name, var) == 0) {
			return *sym;
		}
	}
	return NULL;
}

asymbol *make_bfd_symbol(bfd *abfd, const char *name, int value, int flags, asection *sec) {
	asymbol *ret = bfd_make_empty_symbol(abfd);
	ret->name = name;
	ret->section = sec; // make section returns existing section if found
	ret->flags = flags;
	ret->value = value;
	return ret;
}

asymbol *get_symbol(bfd *abfd, const char *name, int value, int flags, asection *sec) {
	if (lookup_symbol(name) == NULL) {
		append_symbol_to_symbtab(make_bfd_symbol(abfd, name, value, flags, sec));
	}
	return lookup_symbol(name);
}

void append_reloc(bfd *abfd, asection *sec, arelent *rela) {
	DEBUG();

	assert(sec == get_text_section());

	size_t size = sizeof(rela);

	expand_lengthed_array(&rela_text, lengthed_array_size(rela_text) + size);

	*(typedef_ref(arelent *, rela_text.tail)++) = rela;

	/*
	{
		int ret = bfd_install_relocation(abfd, &rela, NULL, 0x00, sec2, NULL);
		if (ret != 2) {
			printf("%i\n", ret);
			abort();
		}
	}
	
	*/
}

// API functions

void mybfd_init(char *objfilename) {
	DEBUG();
	bfd_init();
	abfd = bfd_openw(objfilename, "elf32-marie");
	try(abfd != NULL);
	try(bfd_set_arch_mach(abfd, bfd_arch_marie, 0));
	try(bfd_set_format(abfd, bfd_object));

	try(bfd_set_file_flags(abfd, HAS_DEBUG));
}
__attribute__((destructor)) void mybfd_close(void) {
	DEBUG();

	if (abfd == NULL) // initializer not run
		return;

	// assert(typedef_ref(arelent *, rela_text.head)[0]->sym_ptr_ptr != 0x00);

	asection *text = get_text_section();


	try(bfd_set_symtab(abfd, symtab.head, lengthed_array_size(symtab) / sizeof(asymbol *)));

	add_section_flag(text, SEC_HAS_CONTENTS);

	try(bfd_set_section_size(abfd, text, lengthed_array_size(progdata)));

	// raise(SIGTRAP);
	add_section_flag(text, SEC_RELOC); 
	bfd_set_reloc (abfd, text, rela_text.head, lengthed_array_size(rela_text) / sizeof(arelent *)); // ordering matters here for some reason


	try(bfd_set_section_contents(abfd, text, progdata.head, 0, lengthed_array_size(progdata)));

	try(bfd_close(abfd));
}

void mybfd_set_org(int offset) { // can only happen once
	DEBUG();
	assert(lookup_symbol("ORG") == NULL);
	append_symbol_to_symbtab(make_bfd_symbol(abfd, "ORG", offset, BSF_GLOBAL, bfd_abs_section_ptr));
}

void mybfd_set_source_file(const char *fname) {
	DEBUG();
	append_symbol_to_symbtab(make_bfd_symbol(abfd, fname, 0, BSF_FILE, bfd_abs_section_ptr));
}

void mybfd_set_variable_reloc(const char *var, int ptr) {
	DEBUG();

	reloc_howto_type *ugh = bfd_reloc_type_lookup(abfd, BFD_RELOC_14); // number if bits being moved (address size)

	asymbol **mysymbol = calloc(1, sizeof(asymbol *));
	*mysymbol = get_symbol(abfd, var, -1, BSF_GLOBAL, get_text_section());
	assert(*mysymbol != NULL);

	arelent *rela = calloc(1, sizeof(arelent));
	*rela = (arelent){.sym_ptr_ptr=mysymbol, .address=ptr / 2, .addend=0, .howto=ugh};

	append_reloc(abfd, get_text_section(), rela);
}

void mybfd_set_variable_ref(const char *var, int ptr) { // var must be unique
	DEBUG();
	get_symbol(abfd, var, -1, BSF_GLOBAL, get_text_section())->value = ptr;
}

int mybfd_append_progdata(void * data, size_t size) {
	DEBUG();

	expand_lengthed_array(&progdata, lengthed_array_size(progdata) + size);

	for (char *subdata = data; subdata < (char *)data + size ;subdata++) {
		*typedef_ref(char, progdata.tail)++ = *subdata;
	}

	return size;
}
