%{
#define YYDEBUG 1
#include <stdio.h>
#include "mybfd.h"
#include "arg.h"

int marieerror(char *s);
int marielineno;
int marielex();
FILE *mariein;

static int insn_line_no = 0;

FILE *mapfile;
%}

%token NEWLINE

%token K_ORG

%token K_JNS K_LOAD K_STORE K_ADD K_SUBT K_INPUT K_OUTPUT K_HALT K_SKIPCOND K_JUMP K_CLEAR K_ADDI K_JUMPI

%token K_DEC K_OCT K_HEX 

%union
{
	int number;
	char *string;
	enum {
		I_JNS      = 0x0,
		I_LOAD     = 0x1,
		I_STORE    = 0x2,
		I_ADD      = 0x3,
		I_SUBT     = 0x4,
		I_INPUT    = 0x5,
		I_OUTPUT   = 0x6,
		I_HALT     = 0x7,
		I_SKIPCOND = 0x8,
		I_JUMP     = 0x9,
		I_CLEAR    = 0xa,
		I_ADDI     = 0xb,
		I_JUMPI    = 0xc,
	} opcode;
	struct insn {
		char inst : 4;
		int arg : 12;
	} insn;
}

%token <number> N_DEC N_OCT N_HEX
%token <string> TOKEN


%type<opcode> keyword
%type<string> variable
%type<number> argument

%type<number> memset

%type<string> marker

%type<insn> command

%type<number> org_marker

%type<number> line

%%                   /* beginning of rules section */

assembly: lines
        | org_marker NEWLINE lines /* must be on first non-comment line  or not at all*/

org_marker: K_ORG N_HEX { mybfd_set_org($2); }

lines: /* empty */
     | lines /* empty */ NEWLINE
     | lines line NEWLINE
     | lines error NEWLINE { yyerrok; }

keyword: K_JNS      { $$ = I_JNS; }
       | K_LOAD     { $$ = I_LOAD; }
       | K_STORE    { $$ = I_STORE; }
       | K_ADD      { $$ = I_ADD; }
       | K_SUBT     { $$ = I_SUBT; }
       | K_INPUT    { $$ = I_INPUT; }
       | K_OUTPUT   { $$ = I_OUTPUT; }
       | K_HALT     { $$ = I_HALT; }
       | K_SKIPCOND { $$ = I_SKIPCOND; }
       | K_JUMP     { $$ = I_JUMP; }
       | K_CLEAR    { $$ = I_CLEAR; }
       | K_ADDI     { $$ = I_ADDI; }
       | K_JUMPI    { $$ = I_JUMPI; }


line: marker line { $$ = $2; mybfd_set_variable_ref($1, $2); }
    | command { $$ = insn_line_no++; mybfd_append_progdata((char[2]){$1.inst << 4 | $1.arg  >> 8 & 0xF, // pemdas?
                                                                                   $1.arg & 0xFF},     2); }
    | memset {  $$ = insn_line_no++;  mybfd_append_progdata((char[2]){$1 >> 8, $1 & 0xFF}, 2); }

memset : K_OCT N_OCT { $$ = $2; }
       | K_DEC N_DEC { $$ = $2; }
       | K_HEX N_HEX { $$ = $2; }
       ;

command: keyword argument { $$.inst = $1; $$.arg = $2; }
       | keyword { $$.inst = $1; $$.arg = 0;}

variable: TOKEN { $$ = $1; }

argument: variable { $$ = 0; mybfd_set_variable_reloc($1, insn_line_no * 2 /*+ ??*/); }
        | N_HEX { $$ = $1; }


marker: variable ',' { $$ = $1; }

%%
int main(int argc, char *argv[])
{
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	mariedebug = arguments.debug;
	mariein = arguments.input_filep;

	mybfd_init(arguments.output_file);

	if (arguments.input_file != NULL)
		mybfd_set_source_file(arguments.input_file); /* must be done after init */

	return(marieparse());
}

int marieerror(s)
char *s;
{
  fprintf(stderr, "%i:\t%s\n",marielineno, s);
	return 1;
}

int mariewrap()
{
  return(1);
}
