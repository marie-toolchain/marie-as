#pragma once

#include <stddef.h>

void mybfd_init(char *objfilename);

void mybfd_set_org(int);

void mybfd_set_source_file(const char *);

void mybfd_set_variable_ref(const char*, int);

void mybfd_set_variable_reloc(const char*, int);

int mybfd_append_progdata(void *, size_t);
